FROM ubuntu:20.04

ENV CC65_VERSION 2.19

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -yq \
    apt-transport-https \
    ca-certificates \
    wget \
	build-essential \
	python3 \
	python3-venv \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir /src \
 && cd /src \
 && wget https://github.com/cc65/cc65/archive/refs/tags/V$CC65_VERSION.tar.gz -O cc65.tar.gz \
 && tar -xzvf cc65.tar.gz \
 && rm -f cc65.tar.gz \
 && cd cc65-$CC65_VERSION \
 && make PREFIX=/usr \
 && make install PREFIX=/usr \
 && make clean

