build:
	docker build -t registry.gitlab.com/snopek-games/x16-build-docker:local .

push:
	docker tag registry.gitlab.com/snopek-games/x16-build-docker:local registry.gitlab.com/snopek-games/x16-build-docker:latest
	docker push registry.gitlab.com/snopek-games/x16-build-docker:latest

